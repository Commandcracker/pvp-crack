# PVP Crack
PVP Crack is a Vanilla looking PVP resource pack for Minecraft

## Features
- For Minecraft versions below 1.14.X it adds the textures from 1.14
- Integration of some [VanillaTweaks](https://vanillatweaks.net/picker/resource-packs/) packs
- Small Swords
- Some 3D blocks
- A clean gui
- Moor visible footprints for Hypixel Bedwars 
- Small borders for Wool (this makes fast bridging easier)
- Clean Glass
- Bow Pulling level colors (RED -> YELLOW -> GREEN)
