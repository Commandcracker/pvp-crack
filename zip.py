#!/usr/bin/python3
# coding=utf-8
import zipfile
import os
import json
import tempfile
import threading

version = "2.3"

folders = {
    "1": "1.6.1–1.8.9",
    "2": "1.9–1.10.2",
    "3": "1.11–1.12.2",
    "4": "1.13–1.14.4",
    "5": "1.15–1.16.1",
    "6": "1.16.2–1.16.5",
    "7": "1.17"
}


class SimpleZip:
    def __init__(self, zipf):
        self.zip = zipf

    def addfile(self, file, arc=None):
        print(file, arc)
        self.zip.write(file, arc)

    def adddir(self, path):
        for root, dirs, files in os.walk(path):
            for file in files:
                self.addfile(os.path.join(root, file), os.path.join(root.replace(path, ""), file))


def do_zip(folder):
    temp_dir = tempfile.TemporaryDirectory()
    z = SimpleZip(zipfile.ZipFile("§8PVP §4Crack §a" + folders[folder] + "§7.zip", 'w', zipfile.ZIP_STORED))
    json.dump({
        "pack": {
            "pack_format": int(folder),
            "description": "§cV" + version + "\n§rgitlab.com/Commandcracker"
        }
    }, open(os.path.join(temp_dir.name, "pack.mcmeta"), 'w'), indent=4, sort_keys=True)
    z.addfile("LICENSE.txt")
    z.addfile(os.path.join(temp_dir.name, "pack.mcmeta"), "pack.mcmeta")
    z.adddir('universal')
    z.adddir(folder)
    z.zip.close()
    temp_dir.cleanup()


if __name__ == '__main__':
    threads = list()
    for f in folders:
        x = threading.Thread(target=do_zip, args=f)
        threads.append(x)
        x.start()

    for index, thread in enumerate(threads):
        thread.join()
